const colors = {
  white: '#FFFFFF',
  primary: '#FF8000',
  tertiary: '#FF5511',
  intro_bg: '#f68b26',
  primaryDark: '#000000',
  green: '#6DD230',
  dark: '#252631',
  black1: '#474242',
  background1: '#FFFFFF',
  purple: '#5D10F6',
  red: '#EB5757',
  green2: '#27AE60',
  blue1: '#24A6D5',
  //dika
  orange: '#FF8000',
  orange2: '#FEA33D',
  grayBD: '#bdbdbd',
  gray9E: '#9e9e9e',
  gray82: "#828282",
  grayUnderline: "#e4e4e4",
  orange3: '#ff6d1f',
  bluish: '#F5F4F9',
  
};

export default colors;
