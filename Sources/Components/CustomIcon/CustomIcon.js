import React, { memo } from 'react';
import { View } from 'react-native';
import { Icon } from 'react-native-elements';

const CustomIcon = memo((props) => {
  const { type, name, size, color, onIconPress, style } = props;
  return (
    <View style={style}>
      <Icon
        type={type}
        name={name}
        size={size}
        color={color}
        onPress={onIconPress}
      />
    </View>
  );
});

export default CustomIcon;
