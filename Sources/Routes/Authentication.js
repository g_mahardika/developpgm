import React, {memo} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import { normalize } from 'react-native-elements';
import {defaultFont, width} from '../Constants/constants';
import colors from '../Constants/colors'

const AuthenticationStack = createStackNavigator();

const headerStyle = {
      headerBackTitleVisible: false,
      headerTitleStyle: {
            fontFamily: defaultFont,
            fontSize: normalize(16),
            color: colors.dark,
            fontWeight: '300',
      },
      headerStyle: {
            borderBottomColor: '#fff',
      },
};

const Authentication = memo((props) => {
      return(null)
})

export default Authentication;